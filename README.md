# QQ-Nickname-Generator
一个能生成“xx撤回一条消息并xxxxx”样式昵称的生成器

## 地址

http://api.cirno380a.moe/nickname

## 使用方法

在表单里输入你的昵称和后缀，点击“生成”，程序就会生成一串经过特殊处理的昵称。此时全选生成结果里面的文字，然后复制粘贴到QQ昵称或群名片内。

## 原理

请参阅 https://www.cirno380a.moe/1080.html

## 特别注意

这只是QQ里面的一个BUG，可能在未来会被修复。嘛，能玩一时就玩一时呗。
