/*!
 *  QQ custom retract message generator
 *
 *  Custom JavaScript
 *
 *  by CRH380A-2722
 */

window.onload = function() {

    document.getElementById('generate').onclick = function() {

        var name = document.getElementById('nickName').value;
        var suffix = document.getElementById('nameSuffix').value;

        if (name == '') {
            document.getElementById('error-content').innerHTML = '昵称不能为空';
            document.getElementById('dialog').style.display = 'block';
        } else if (suffix == '') {
            document.getElementById('error-content').innerHTML = '后缀不能为空';
            document.getElementById('dialog').style.display = 'block';
        } else {

            suffix = "并" + suffix;

            suffix = suffix.split("").reverse().join("");

            var result = name + String.fromCharCode('8238') + suffix + String.fromCharCode('8237');

            document.getElementById('result').value = result;

            document.getElementById('resultDiv').style.display = 'block';

        }

    };

    document.getElementById('dialog-close').onclick = function() {
        document.getElementById('dialog').style.display = 'none';
    };

};
